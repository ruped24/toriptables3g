# toriptables3g with GUI Desktop notification  

![](https://img.shields.io/badge/toriptables3g-python%203.8-blue.svg?style=flat-square) ![](https://img.shields.io/badge/dependencies-tor_-orange.svg?style=flat-square) ![](https://img.shields.io/badge/GPL-v2-blue.svg?style=flat-square)



Tor Iptables script is an anonymizer that sets up iptables and tor to route all services and traffic including DNS through the Tor network.
***

### Dependencies:

`sudo apt install tor`

### Usage: ###

`toriptables3g.py -h`


### Screenshots: ###

* [Kali Linux, Rolling Edition](https://drive.google.com/file/d/136xanPctr35oHxshW5abVGyFg0cslzV4/view?usp=sharing)

* [Kali Linux, Rolling Edition](https://drive.google.com/file/d/1lDKIrx4XQpObjHUJ2t9ESa93z9XD4Shd/view?usp=sharing)

* [Tor IPTables rules loaded](https://drive.google.com/open?id=0B79r4wTVj-CZT0NMV2VZRTM1REE)


### To test: ###

* [Check My IP](http://check-my-ip.net)

* [Check Tor Project](https://check.torproject.org)
 
* [Witch proxy checker](http://witch.valdikss.org.ru)

* [Do I leak](http://www.doileak.com/)
 
* [DNS leak test](http://dnsleaktest.com)

* [What every Browser knows about you](http://webkay.robinlinus.com/)

### To manually change IP w/o reload:
Refresh Check Tor Project webpage

```
#!bash

sudo kill -HUP $(pidof tor)
```

****
### [Troubleshooting](https://github.com/ruped24/toriptables2/wiki/Troubleshooting)
****